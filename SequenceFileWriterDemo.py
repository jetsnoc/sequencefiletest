#!/usr/bin/env python

from hadoop.io.SequenceFile import CompressionType
from hadoop.io import LongWritable
from hadoop.io import SequenceFile
from hadoop.io import Text
import glob
import os

def writeData(writer):
    key = Text()
    value = Text()
    for filename in glob.glob(os.path.join('/Users/brian/workspace/Hadoop/python-hadoop/posbo', '*.xml')):
        i=0;
        f=open(filename, 'r')
        key.set(filename)
        value.set(f.read())
        writer.append(key, value)

if __name__ == '__main__':
    writer = SequenceFile.createWriter('test.seq', Text, Text)
    writeData(writer)
    writer.close()

    writer = SequenceFile.createWriter('test-record.seq', Text, Text, compression_type=CompressionType.RECORD)
    writeData(writer)
    writer.close()

    writer = SequenceFile.createWriter('test-block.seq', Text, Text, compression_type=CompressionType.BLOCK)
    writeData(writer)
    writer.close()

